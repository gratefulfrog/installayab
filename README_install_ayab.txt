Il y avait plusieurs problèmes liés tous aux contradictions dans les versions
de PyQt, Python, Ayab.

Pour faire marchere les scripts : 

1. Download les scripts dans un répertoire où tu veux installer
   ayab version 0.90.
   On va créer un répertoire AYAB_09, puis l'installer correctement (si on peut
   utiliser ce mot pour ayab...) puis le lancer.

2. $ chmod a+x  *.sh

3. $ ./install_ayab_0.90.sh

Ensuite, pour lancer ayab à chaque fois qu'on veut :

4. $ ./run_ayab_0.90.sh

