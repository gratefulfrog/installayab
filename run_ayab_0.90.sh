#!/bin/bash


VERSION=0.9
INSTALL=AYAB_$VERSION
TARGET=ayab==$VERSION

echo "*****"
echo "*****"

echo "First cd to the installation" 
cd $INSTALL

echo "*****"
echo "*****"

echo "Now start the virtual environment"
echo "*****"
echo "*****"

source venv/bin/activate

echo "*****"
echo "*****"

echo "Now run ayab"

echo "*****"
echo "*****"

ayab

echo "*****"
echo "*****"

echo "Now exiting, goode-bye..."
deactivate
