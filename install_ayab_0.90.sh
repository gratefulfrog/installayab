#!/bin/bash

VERSION=0.9
INSTALL=AYAB_$VERSION
TARGET=ayab==$VERSION

echo "*****"
echo "*****"

echo "First be sure that everything is installed and up to date"

echo "*****"
echo "*****"

sudo apt-get install python3-pip python3-pyqt5 python3-dev python3-virtualenv
sudo python3 -m pip install --upgrade pip

echo "*****"
echo "*****"

echo "Now create a working directory"
mkdir -p $INSTALL
cd $INSTALL

echo "*****"
echo "*****"

echo "Now create the virtual environment"
echo "*****"
echo "*****"

virtualenv -p python3.6 --system-site-packages venv/

echo "*****"
echo "*****"

echo "Now installing ayab..."
source venv/bin/activate
python3 -m pip install $TARGET

echo "*****"
echo "*****"

echo "Now running ayab"
echo "*****"
echo "*****"

ayab

echo "*****"
echo "*****"

echo "Now exiting, goode-bye and good luck with it..."

echo "*****"
echo "*****"

deactivate
